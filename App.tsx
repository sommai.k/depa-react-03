import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Provider } from 'react-redux';

// custom component
import { Login } from "./components/Login";
import { Register } from './components/Register';
import { Main } from './components/Main';
import { HelloWorld } from './components/HelloWorld';
import { store } from './app/store';
import { Post } from './components/Post';

const Stack = createNativeStackNavigator();

export default function App() {
  return <>
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="login"
            component={Login}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="register"
            component={Register}
            options={{ title: "ลงทะเบียน" }}
          />
          <Stack.Screen
            name="main"
            component={Main}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="helloworld"
            component={HelloWorld}
            options={{ title: "สวัสดีชาวโลก" }}
          />
          <Stack.Screen
            name="post"
            component={Post}
            options={{ title: "บทความ" }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  </>
}