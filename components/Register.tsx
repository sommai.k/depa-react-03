import { FC } from 'react';
import { View, Text, Pressable, Alert } from 'react-native';
import { z } from 'zod';
import { useForm, SubmitHandler } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod'
import { MyInput } from '../components/ui/MyInput';
import { useNavigation, NavigationProp } from '@react-navigation/native';

const RegisterSchema = z.object({
    userName: z.string().min(5).max(150),
    firstName: z.string().max(150),
    lastName: z.string().max(150),
    password: z.string().min(8).max(25),
});

type RegisterModel = z.infer<typeof RegisterSchema>;

export const Register: FC = () => {
    // hooks
    const { control, handleSubmit, reset } = useForm<RegisterModel>({
        resolver: zodResolver(RegisterSchema)
    });

    const navigation = useNavigation<NavigationProp<any>>();

    // when validate pass
    const validatePass: SubmitHandler<RegisterModel> = async (form) => {
        console.log(form);
        try {
            // logic
            const resp = await fetch(
                "http://18.141.13.245/users",
                {
                    method: 'POST',
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify({
                        "userName": form.userName,
                        "firstName": form.firstName,
                        "lastName": form.lastName,
                        "password": form.password
                    })
                }
            )
            if (resp.status === 201) {
                Alert.alert("สำเร็จ", "ลงทะเบียนผู้ใช้สำเร็จ");
                navigation.navigate("login");
            } else if (resp.status === 200) {
                Alert.alert("ไม่สำเร็จ", "รหัสพนักงานซ้ำ");
            }
        } catch (e) {
            // when error
            console.log(e);
            Alert.alert("ระบบมีปัญหา", "กรุณาลองอีกครั้ง");
        }
    }
    // when reset press
    const resetForm = () => {
        reset();
    }

    return <>
        <View className='w-full h-full px-4 space-y-4'>
            <View>
                <MyInput label='รหัสพนักงาน' name='userName' control={control} />
            </View>
            <View>
                <MyInput label='ชื่อ' name='firstName' control={control} />
            </View>
            <View>
                <MyInput label='นามสกุล' name='lastName' control={control} />
            </View>
            <View>
                <MyInput label='รหัสผ่าน' name='password' control={control} />
            </View>
            <View className='flex flex-row space-x-2'>
                <Pressable onPress={handleSubmit(validatePass)}
                    className='bg-blue-600 p-2 rounded-md flex-1'>
                    <Text className='text-white text-center'>ลงทะเบียน</Text>
                </Pressable>
                <Pressable onPress={resetForm}
                    className='bg-red-600 p-2 rounded-md flex-1'>
                    <Text className='text-white text-center'>ยกเลิก</Text>
                </Pressable>
            </View>
        </View>
    </>
}