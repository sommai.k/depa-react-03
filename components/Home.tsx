import { FC } from 'react';
import { View, Text } from 'react-native';
import { useSelector } from 'react-redux';

export const Home: FC = () => {
    // hooks
    const { firstName, lastName } = useSelector((store: any) => store.user);
    return <>
        <View>
            <Text>Home Screen</Text>
            <Text>สวัสดีคุณ {firstName} {lastName}</Text>
        </View>
    </>
}