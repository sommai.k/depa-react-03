import { FC } from 'react';
import { View, Text, Image, Pressable, Alert } from 'react-native';
import { useNavigation, NavigationProp } from '@react-navigation/native';
import { z } from 'zod';
import { zodResolver } from '@hookform/resolvers/zod';
import { useForm, SubmitHandler } from 'react-hook-form';
import { useDispatch } from 'react-redux';

import { MyInput } from './ui/MyInput';
import { setFirstName, setLastName } from '../app/user-slice';

const LoginSchema = z.object({
    userName: z.string({ required_error: "กรุณาระบุรหัสพนักงาน" }),
    password: z.string({ required_error: "กรุณาระบุรหัสผ่าน" })
});

type LoginModel = z.infer<typeof LoginSchema>;

export const Login: FC = () => {
    // hooks
    const navigation = useNavigation<NavigationProp<any>>();
    const { control, reset, handleSubmit } = useForm<LoginModel>({
        resolver: zodResolver(LoginSchema)
    });
    const dispatch = useDispatch();

    // navigate to Register Screen
    const whenGotoRegister = () => {
        navigation.navigate("register");
    }

    // when validate pass
    const validatePass: SubmitHandler<LoginModel> = async (form) => {
        console.log(form);
        // send form data to api server
        try {
            const resp = await fetch(
                "http://18.141.13.245/login",
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "userName": form.userName,
                        "password": form.password
                    })
                }
            );
            if (resp.status === 200) {
                const jsonResp = await resp.json();
                if (jsonResp.success) {
                    dispatch(setFirstName(jsonResp.firstName));
                    dispatch(setLastName(jsonResp.lastName));
                    navigation.navigate("main");
                } else {
                    Alert.alert("เข้าระบบไม่สำเร็จ", "กรุณาลองอีกครั้ง");
                }
            } else {
                Alert.alert("เข้าระบบไม่สำเร็จ", "กรุณาลองอีกครั้ง");
            }
        } catch (e) {
            console.log(e);
            Alert.alert("ระบบมีปัญหา", "กรุณาแจ้งเจ้าหน้าที่");
        }
    }
    // when reset press
    const resetForm = () => {
        reset();
    }

    return <>
        <View className='w-full h-full px-4 space-y-4'>
            <View className='flex flex-col items-center mt-10'>
                <Image
                    className='rounded-full'
                    source={require('../assets/logo.jpg')} />
                <Text
                    className='text-3xl text-blue-800 font-bold'>ยินดีต้อนรับ</Text>
            </View>
            <View>
                <MyInput label='รหัสพนักงาน' name="userName" control={control} />
            </View>
            <View>
                <MyInput label='รหัสผ่าน' name="password" control={control} isSecure={true} />
            </View>
            <View className='flex flex-row space-x-2'>
                <Pressable onPress={handleSubmit(validatePass)}
                    className='bg-blue-600 p-2 rounded-md flex-1'>
                    <Text className='text-white text-center'>เข้าสู่ระบบ</Text>
                </Pressable>
                <Pressable onPress={resetForm}
                    className='bg-red-600 p-2 rounded-md flex-1'>
                    <Text className='text-white text-center'>ยกเลิก</Text>
                </Pressable>
            </View>
            <View>
                <Pressable onPress={whenGotoRegister}>
                    <Text
                        className='text-blue-700 text-center underline'>ลงทะเบียนผู้ใช้ใหม่</Text>
                </Pressable>
            </View>
        </View>
    </>
}