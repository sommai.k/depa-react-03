import { FC, Fragment, useState } from 'react';
import { Text, View, Button } from 'react-native';

interface Props {
    title?: string;
    // name: string;
}

export const HelloWorld: FC<Props> = (p) => {
    // declare variable
    const [cnt, setCnt] = useState(10);

    // custom function
    const whenAddPress = () => {
        setCnt(cnt + 1);
    }

    return <Fragment>
        <View className='w-full h-full p-8 items-center'>
            <View className='my-auto'>
                <Text className='text-2xl text-blue-600'>
                    {p.title ?? 'Hello'} {cnt}</Text>
                <Button title='Add' onPress={whenAddPress} />
            </View>
        </View>
    </Fragment>
}