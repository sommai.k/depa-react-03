import { FC, useState, useEffect } from 'react';
import { View, Text, Alert, ScrollView } from 'react-native';

export const Post: FC = () => {
    // hooks
    const [posts, setPosts] = useState([]);
    // load data from server
    const loadData = async () => {
        try {
            const resp = await fetch("https://jsonplaceholder.typicode.com/posts");
            if (resp.status === 200) {
                const jsonResp = await resp.json();
                setPosts(jsonResp);
            } else {
                Alert.alert("ระบบมีปัญหา", "กรุณาลองใหม่อีกครั้ง");
            }
        } catch (e) {
            Alert.alert("ระบบมีปัญหา", "กรุณาลองใหม่อีกครั้ง");
        }
    }
    useEffect(() => {
        loadData();
    }, []);
    return <>
        <ScrollView className='w-full h-full' >
            {posts.map((d, i) =>
                <View key={i}
                    className='flex flex-row items-center p-4 border-b-2 border-green-100 bg-white'>
                    <View className='w-2/12'>
                        <Text className='text-3xl text-center'>{d['id']}</Text>
                    </View>
                    <View className='flex-1'>
                        <Text className='text-xl'>{d['title']}</Text>
                        <Text className='text-sm'>{d['body']}</Text>
                    </View>
                </View>
            )}
        </ScrollView>
    </>
}