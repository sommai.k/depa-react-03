import { FC } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialIcons } from '@expo/vector-icons';
import { Home } from './Home';
import { Menu } from './Menu';
import { Message } from './Message';
import { Setting } from './Setting';

const BottomTab = createBottomTabNavigator();

export const Main: FC = () => {
    return <>
        <BottomTab.Navigator>
            <BottomTab.Screen
                name='home'
                component={Home}
                options={{
                    title: 'หน้าหลัก',
                    tabBarIcon: ({ color, size }) => (
                        <MaterialIcons name='home' color={color} size={size} />
                    )
                }}
            />
            <BottomTab.Screen
                name='menu'
                component={Menu}
                options={{
                    title: 'เมนู',
                    tabBarIcon: ({ color, size }) => (
                        <MaterialIcons name='menu' color={color} size={size} />
                    )
                }}
            />
            <BottomTab.Screen
                name='message'
                component={Message}
                options={{
                    title: 'ข้อความ',
                    tabBarIcon: ({ color, size }) => (
                        <MaterialIcons name='message' color={color} size={size} />
                    )
                }}
            />
            <BottomTab.Screen
                name='setting'
                component={Setting}
                options={{
                    title: 'ตั้งค่า',
                    tabBarIcon: ({ color, size }) => (
                        <MaterialIcons name='settings' color={color} size={size} />
                    )
                }}
            />
        </BottomTab.Navigator>
    </>
}