import { FC } from 'react';
import { View, Text, Pressable } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useNavigation, NavigationProp } from '@react-navigation/native';

export const Menu: FC = () => {
    const navigation = useNavigation<NavigationProp<any>>();
    // when goto hello
    const gotoHello = () => {
        navigation.navigate('helloworld');
    }
    const gotoPost = () => {
        navigation.navigate('post');
    }
    return <>
        <View className='w-full h-full flex flex-row flex-wrap'>

            <Pressable onPress={gotoHello}
                className='flex flex-col w-1/3 items-center p-4'>
                <MaterialCommunityIcons name='earth' size={64} color="#B0EBB4" />
                <Text>Hello World</Text>
            </Pressable>

            <View className='flex flex-col w-1/3 items-center p-4'>
                <MaterialCommunityIcons name='account' size={64} color="#EADFB4" />
                <Text>User</Text>
            </View>
            <View className='flex flex-col w-1/3 items-center p-4'>
                <MaterialCommunityIcons name='list-status' size={64} color="#B3C8CF" />
                <Text>Todo</Text>
            </View>
            <View className='flex flex-col w-1/3 items-center p-4'>
                <MaterialCommunityIcons name='camera-iris' size={64} color="#E1ACAC" />
                <Text>Photo</Text>
            </View>
            <View className='flex flex-col w-1/3 items-center p-4'>
                <MaterialCommunityIcons name='image-album' size={64} color="#CAB69E" />
                <Text>Album</Text>
            </View>
            <View className='flex flex-col w-1/3 items-center p-4'>
                <MaterialCommunityIcons name='wechat' size={64} color="#F6995C" />
                <Text>Comment</Text>
            </View>
            <Pressable onPress={gotoPost}
                className='flex flex-col w-1/3 items-center p-4'>
                <MaterialCommunityIcons name='pencil' size={64} color="#51829B" />
                <Text>Post</Text>
            </Pressable>
        </View>
    </>
}